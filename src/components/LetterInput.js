import React, { Component } from "react";

export default class LetterInput extends Component {
  componentDidMount() {
    this.letterInput.focus();
  }
  onKeyPress(event) {
    this.props.onKeyPress(event.key);
    event.preventDefault();
  }
  render() {
    return (
      <div>
        <input
          ref={(input) => {
            this.letterInput = input;
          }}
          type="text"
          placeholder="Input letter"
          onKeyPress={(event) => this.onKeyPress(event)}
        />
      </div>
    );
  }
}
