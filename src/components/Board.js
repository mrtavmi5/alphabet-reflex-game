import React, { Component } from "react";
import LetterKey from "./LetterKey";
import { LocalContext } from "../App";
import { LETTERS } from "../App";

export default class Board extends Component {
  render() {
    const letters = LETTERS.split("");

    return (
      <LocalContext.Consumer>
        {(app) => (
          <div className="board">
            <ul>
              {letters.map((value, index) => (
                <li key={index}>
                  <LetterKey
                    value={value}
                    index={index}
                    status={app.state.status[value]}
                  />
                </li>
              ))}
            </ul>
          </div>
        )}
      </LocalContext.Consumer>
    );
  }
}
