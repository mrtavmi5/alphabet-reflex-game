import React, { Component } from "react";

export default class LetterCode extends Component {
  render() {
    return <div className="letter-code">{this.props.value}</div>;
  }
}
