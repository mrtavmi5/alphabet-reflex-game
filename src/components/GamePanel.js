import React, { Component } from "react";
import LetterCode from "./LetterCode";
import LetterInput from "./LetterInput";
import { LocalContext } from "../App";

export default class GamePanel extends Component {
  render() {
    return (
      <LocalContext.Consumer>
        {(app) =>
          app.state.numbers.length ? (
            <div className="game-panel">
              <LetterCode value={app.state.numbers[0]} />
              <LetterInput onKeyPress={(value) => app.checkResult(value)} />
            </div>
          ) : null
        }
      </LocalContext.Consumer>
    );
  }
}
