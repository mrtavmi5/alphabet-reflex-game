import React, { Component } from "react";
import { LocalContext } from "../App";

export default class Score extends Component {
  render() {
    return (
      <LocalContext.Consumer>
        {(app) => (
          <div className="score">
            <table class="table">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">SCORE</th>
                </tr>
              </thead>
              <tbody>
                <tr class="table-success">
                  <td>HIT: {app.state.hits}</td>
                </tr>
                <tr class="table-danger">
                  <td>MISS: {app.state.misses}</td>
                </tr>
                <tr lass="table-light">
                  <td>LEFT: {app.state.left}</td>
                </tr>
              </tbody>
            </table>
          </div>
        )}
      </LocalContext.Consumer>
    );
  }
}
