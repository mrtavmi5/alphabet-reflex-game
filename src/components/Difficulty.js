import React, { Component } from "react";
import { LocalContext } from "../App";

export default class Difficulty extends Component {
  render() {
    return (
      <LocalContext.Consumer>
        {(app) =>
          !app.state.numbers.length ? (
            <div
              className="difficulty"
              onChange={(e) => app.setDifficulty(e.target.value)}
            >
              <div>
                <input type="radio" id="easy" name="difficulty" value="5000" />
                <label>EASY</label>
              </div>
              <div>
                <input
                  type="radio"
                  id="medium"
                  name="difficulty"
                  value="3500"
                />
                <label>MEDIUM</label>
              </div>
              <div>
                <input type="radio" id="hard" name="difficulty" value="2000" />
                <label>HARD</label>
              </div>
            </div>
          ) : null
        }
      </LocalContext.Consumer>
    );
  }
}
