import React, { Component } from "react";
import Board from "./Board";
import Difficulty from "./Difficulty";
import GamePanel from "./GamePanel";
import Start from "./Start";

export default class MainPanel extends Component {
  render() {
    return (
      <div className="main-panel">
        <Difficulty />
        <Start />
        <GamePanel />
        <Board />
      </div>
    );
  }
}
