import React, { Component } from "react";

export default class LetterKey extends Component {
  render() {
    let className = "letter-key ";
    switch (this.props.status) {
      case true:
        className += "success";
        break;
      case false:
        className += "fail";
        break;
      default:
        break;
    }

    return (
      <div className={className}>
        <span>{this.props.value.toUpperCase()}</span>
        <span>({this.props.index + 1})</span>
      </div>
    );
  }
}
