import React, { Component } from "react";
import { LocalContext } from "../App";

export default class Start extends Component {
  render() {
    return (
      <LocalContext.Consumer>
        {(app) =>
          app.state.startTimerValue ? (
            <button
              type="button"
              value="start"
              class="btn btn-primary btn-lg"
              onClick={() => app.toggleGame()}
            >
              {app.state.numbers.length ? "Stop" : "Start Game"}
            </button>
          ) : null
        }
      </LocalContext.Consumer>
    );
  }
}
