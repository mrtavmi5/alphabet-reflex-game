import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import MainPanel from "./components/MainPanel";
import Score from "./components/Score";

export const LocalContext = React.createContext();
export const LETTERS = "abcdefghijklmnopqrstuvwxyz";

class App extends React.Component {
  state = {
    hits: 0,
    misses: 0,
    left: 0,
    numbers: [],
    startTimerValue: 0,
    status: {},
  };

  timeout = null;

  makeNewArray() {
    return LETTERS.split("")
      .map((value, index) => index + 1)
      .sort((a, b) => 0.5 - Math.random());
  }

  toggleGame() {
    if (this.state.numbers.length) {
      this.setState({ numbers: [] });
      if (this.timeout) {
        clearTimeout(this.timeout);
      }
    } else {
      this.reset();
    }
  }

  reset() {
    this.setState({
      hits: 0,
      misses: 0,
      left: LETTERS.length,
      numbers: this.makeNewArray(),
      status: {},
    });
    this.timeout = setTimeout(
      () => this.checkResult(),
      this.state.startTimerValue
    );
  }

  setDifficulty(startTimerValue) {
    this.setState({ startTimerValue });
  }

  checkResult(value) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    const currentNumber = this.state.numbers[0];
    const currentLetter = LETTERS[currentNumber - 1];
    const newState = {
      left: this.state.left - 1,
      numbers: this.state.numbers.slice(1),
      status: { ...this.state.status },
    };
    if (currentLetter === value) {
      newState.hits = this.state.hits + 1;
      newState.status[currentLetter] = true;
    } else {
      newState.misses = this.state.misses + 1;
      newState.status[currentLetter] = false;
    }
    this.setState(newState);
    if (newState.numbers.length) {
      this.timeout = setTimeout(
        () => this.checkResult(),
        this.state.startTimerValue
      );
    }
  }

  render() {
    return (
      <LocalContext.Provider value={this}>
        <div className="app">
          <MainPanel />
          <Score />
        </div>
      </LocalContext.Provider>
    );
  }
}

export default App;
